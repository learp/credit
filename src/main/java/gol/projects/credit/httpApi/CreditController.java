package gol.projects.credit.httpApi;

import gol.projects.credit.model.payment.ConstantPayment;
import gol.projects.credit.model.CreditParams;
import gol.projects.credit.model.payment.NullPayment;
import gol.projects.credit.model.payment.PaymentParams;
import gol.projects.credit.model.report.PaymentReport;
import gol.projects.credit.services.AnnuityCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@RestController
@RequestMapping("/credit")
public class CreditController {

    @RequestMapping(
        value = "/annuity",
        method = GET)
    public PaymentReport annuity(@RequestParam double sumOfCredit,
                                 @RequestParam double percent,
                                 @RequestParam int duration,
                                 @RequestParam(required = false) Double realPayment) {

        CreditParams creditParams = new CreditParams(sumOfCredit, percent, duration);
        PaymentParams paymentParams = realPayment == null ? new NullPayment() : new ConstantPayment(realPayment);

        return annuityCreditService.calculate(creditParams, paymentParams);
    }

    @Autowired
    AnnuityCreditService annuityCreditService;
}
