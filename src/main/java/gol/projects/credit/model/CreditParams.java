package gol.projects.credit.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@AllArgsConstructor
@Getter
public class CreditParams {
    private final double sumOfCredit;
    private final double percent;
    private final int durationMonth;
}
