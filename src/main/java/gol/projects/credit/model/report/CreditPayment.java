package gol.projects.credit.model.report;

public interface CreditPayment extends Report {
    double getTotal();
    double getPercents();
}
