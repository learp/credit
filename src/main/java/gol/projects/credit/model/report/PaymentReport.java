package gol.projects.credit.model.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@JsonSerialize
@JsonInclude(NON_NULL)
@Getter
public class PaymentReport implements CreditPayment {

    public void addReport(CreditPayment paymentReport) {
        if (reports == null) {
            reports = new ArrayList<>();
        }

        reports.add(paymentReport);
        total += paymentReport.getTotal();
        total += paymentReport.getPercents();
    }

    private List<CreditPayment> reports;
    private double total;
    private double percents;
}
