package gol.projects.credit.model.report;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.ZonedDateTime;

@AllArgsConstructor
@Getter
public class MonthlyReport implements CreditPayment {
    private ZonedDateTime zonedDateTime;
    private double total;
    private double percents;
}
