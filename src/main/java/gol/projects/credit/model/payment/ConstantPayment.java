package gol.projects.credit.model.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@AllArgsConstructor
@Getter
public class ConstantPayment implements PaymentParams {
    private double payment;
}
