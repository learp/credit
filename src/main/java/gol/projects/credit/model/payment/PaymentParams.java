package gol.projects.credit.model.payment;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
public interface PaymentParams {
    double getPayment();
}
