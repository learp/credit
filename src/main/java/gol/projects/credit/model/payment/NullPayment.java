package gol.projects.credit.model.payment;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
public class NullPayment implements PaymentParams {
    @Override
    public double getPayment() {
        return 0;
    }
}
