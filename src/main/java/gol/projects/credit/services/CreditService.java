package gol.projects.credit.services;

import gol.projects.credit.model.CreditParams;
import gol.projects.credit.model.payment.PaymentParams;
import gol.projects.credit.model.report.PaymentReport;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
public interface CreditService {
    PaymentReport calculate(CreditParams creditParams, PaymentParams paymentParams);
}
