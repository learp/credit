package gol.projects.credit.services;

import gol.projects.credit.model.CreditParams;
import gol.projects.credit.model.payment.PaymentParams;
import gol.projects.credit.model.report.PaymentReport;
import org.springframework.stereotype.Component;

@Component
public class DifferCreditService implements CreditService {

    @Override
    public PaymentReport calculate(CreditParams creditParams, PaymentParams paymentParams) {

        double sumOfCredit = 5070000;
        double percentByYear = 0.0985;
        double percentByMonth = percentByYear / 12;
        int duration = 220;

//        double divide = 1.0 / Math.pow(1 + percentByMonth, duration);
//        double subtract = 1.0 - divide;
        double annuityPayment = sumOfCredit/duration;
        double percentPayment = sumOfCredit * percentByMonth;
        double total = (annuityPayment + percentPayment) * duration;

        System.out.println("Total: " + total);

        double sum = 0;
        double sumPercents = 0;
        double sumBodyPayment = 0;
        double truePay = 120000;

        int durationLeft = duration;
        while (durationLeft > 0) {
//            divide = 1.0 / Math.pow(1 + percentByMonth, durationLeft);
//            subtract = 1.0 - divide;
            double bodyPayment = sumOfCredit/durationLeft;
            percentPayment = sumOfCredit * percentByMonth /** 30.4166667 / 365.3*/;
            annuityPayment = bodyPayment + percentPayment;


//            if (durationLeft % 3 == 0) {
//                truePay += 47000;
//            }
//
//            if (durationLeft % 12 == 0) {
//                truePay += 43000;
//            }

            System.out.println("Month " + (duration - durationLeft + 1) + " ----------------------");
            System.out.println("Percent: " + percentPayment);
            System.out.println("Main annuityPayment: " + bodyPayment);
            System.out.println("Payment: " + annuityPayment);
            System.out.println("SumOfCredit: " + sumOfCredit);

            sumPercents += percentPayment;

//            sumOfCredit -= bodyPayment;
//            sum += annuityPayment;
//            sumBodyPayment += bodyPayment;
//
            if (sumOfCredit < truePay) {
                sum += sumOfCredit;
                sumBodyPayment += sumOfCredit;
                System.out.println("TruPayment: " + sumOfCredit);

                sumOfCredit = 0;
                break;
            }
            else {
                sumOfCredit -= (truePay - percentPayment);
                sum += truePay;
                sumBodyPayment += (truePay - percentPayment);

                System.out.println("TruPayment: " + truePay);
            }

//            if (durationLeft % 3 == 0) {
//                truePay -= 47000;
//            }
//
//            if (durationLeft % 12 == 0) {
//                truePay -= 43000;
//            }

            // if (durationLeft % 3 == 0) {
            // sumOfCredit -= 47000;
            // sum += 47000;
            // sumBodyPayment += 47000;
            // }

            durationLeft--;
        }

        System.out.println("-------------------------------");

        System.out.println("Total: " + sum);
        System.out.println("Body: " + sumBodyPayment);
//        System.out.println("Diff: " + (total - sum));
        System.out.println("Proc: " + sumPercents);
        return null;
    }
}
