package gol.projects.credit.services;

import gol.projects.credit.model.CreditParams;
import gol.projects.credit.model.payment.PaymentParams;
import gol.projects.credit.model.report.CreditPayment;
import gol.projects.credit.model.report.MonthlyReport;
import gol.projects.credit.model.report.PaymentReport;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@Component
public class AnnuityCreditService implements CreditService {

    public PaymentReport calculate(CreditParams creditParams, PaymentParams paymentParams) {
        PaymentReport paymentReport = new PaymentReport();

        double sumOfCredit = creditParams.getSumOfCredit();
        double percentByYear = creditParams.getPercent();
        double percentByMonth = percentByYear / 12;
        int duration = creditParams.getDurationMonth();

        double sum = 0;
        double sumPercents = 0;
        double sumBodyPayment = 0;
        double truePay = paymentParams.getPayment();

        int durationLeft = duration;
        while (durationLeft > 0) {
            double divide = 1.0 / Math.pow(1 + percentByMonth, durationLeft);
            double subtract = 1.0 - divide;

            double annuityPayment = sumOfCredit * percentByMonth / subtract;

            double percentPayment = sumOfCredit * percentByYear * 30.4166667 / 365.3;
            double bodyPayment = annuityPayment - percentPayment + paymentParams.getPayment();

            CreditPayment monthlyReport = new MonthlyReport(ZonedDateTime.now(), bodyPayment, percentPayment);
            paymentReport.addReport(monthlyReport);

            sumPercents += percentPayment;

            sumOfCredit -= bodyPayment;
            sum += annuityPayment;
            sumBodyPayment += bodyPayment;
//
//            if (sumOfCredit < truePay) {
//                sum += sumOfCredit;
//                sumBodyPayment += sumOfCredit;
//                System.out.println("TruPayment: " + sumOfCredit);
//
//                sumOfCredit = 0;
//                break;
//            }
//            else {
//                sumOfCredit -= (truePay - percentPayment);
//                sum += truePay;
//                sumBodyPayment += (truePay - percentPayment);
//
//                System.out.println("TruPayment: " + truePay);
//            }

//            if (durationLeft % 3 == 0) {
//                truePay -= 47000;
//            }
//
//            if (durationLeft % 12 == 0) {
//                truePay -= 43000;
//            }

            // if (durationLeft % 3 == 0) {
            // sumOfCredit -= 47000;
            // sum += 47000;
            // sumBodyPayment += 47000;
            // }

            durationLeft--;
        }

//        System.out.println("-------------------------------");
//
//        System.out.println("Total: " + sum);
//        System.out.println("Body: " + sumBodyPayment);
//        System.out.println("Diff: " + (total - sum));
//        System.out.println("Proc: " + sumPercents);

        return paymentReport;
    }
}
